#### NOTES ON main.tf 'local-exec' provisioner

- Instead of `user_data` arg to `aws_instance` resource, used `local-exec` provisioner to run Ansible playbook in `docker-deploy-demo` proj to configure newly created EC2.
