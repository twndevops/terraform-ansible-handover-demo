provider "aws" {
  region = "us-east-1"
}

variable "vpc_cidr_block" {
  default="10.0.0.0/16"
}
variable "subnet_cidr_block" {
  default="10.0.10.0/24"
}
variable "az" {
  default="us-east-1d"
}
variable "env_prefix" {
  default="dev"
}
variable "my_ip" {
  default="172.56.71.198/32"
}
variable "instance_type" {
  default="t2.micro"
}
# variable "public_key" {}
variable "public_key_path" {
  default="~/.ssh/id_rsa_no_pw.pub"
}
variable "private_key_path" {
  default="~/.ssh/id_rsa_no_pw"
}

resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name: "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "myapp-subnet-1" {
  vpc_id = aws_vpc.myapp-vpc.id
  cidr_block = var.subnet_cidr_block
  availability_zone = var.az
  tags = {
    Name: "${var.env_prefix}-subnet-1"
  }
}

# FOLLOWING creates new RTB and new IGW for custom VPC created above

# resource "aws_route_table" "myapp-rtb" {
#   vpc_id = aws_vpc.myapp-vpc.id

#   # Route handling in-VPC traffic was created w/ custom VPC above

#   # Route handling traffic in/out of VPC via Internet Gateway
#   route {
#     # any IP
#     cidr_block = "0.0.0.0/0"
#     # IGW ID
#     gateway_id = aws_internet_gateway.myapp-igw.id
#   }

#   tags = {
#     Name = "${var.env_prefix}-rtb"
#   }
# }

resource "aws_internet_gateway" "myapp-igw" {
  vpc_id = aws_vpc.myapp-vpc.id

  tags = {
    Name = "${var.env_prefix}-igw"
  }
}

# resource "aws_route_table_association" "myapp-rtb-subnet-assoc" {
#   subnet_id = aws_subnet.myapp-subnet-1.id
#   route_table_id = aws_route_table.myapp-rtb.id
# }

# FOLLOWING creates new Internet Route in existing main rtb for custom VPC created above, using IGW created above
# ALSO applies Name tag to existing main rtb
resource "aws_default_route_table" "myapp-main-rtb" {
  # no need to supply vpc_id since main rtb exists in VPC already
  default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id # or main_route_table_id

  # in-VPC traffic handling Route already exists in main rtb

  route {
    # any IP
    cidr_block = "0.0.0.0/0"
    # IGW ID
    gateway_id = aws_internet_gateway.myapp-igw.id
  }

  tags = {
    Name: "${var.env_prefix}-main-rtb"
  }
}

# FOLLOWING creates new Sec Group

# resource "aws_security_group" "myapp-sg" {
#   name = "myapp-sg"
  
#   # must assoc Sec Group to VPC
#   vpc_id = aws_vpc.myapp-vpc.id

#   ingress {
#     description = "SSH into EC2"
#     # define port range to open to traffic
#     from_port = 22
#     to_port = 22
#     protocol = "tcp"
#     # set your IP to access port(s) opened above
#     cidr_blocks = [var.my_ip]
#   }

#   ingress {
#     description = "Browser access"
#     from_port = 8080
#     to_port = 8080
#     protocol = "tcp"
#     # any IP can access from browser
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   egress {
#     description = "All outbound traffic to Internet"
#     from_port = 0
#     to_port = 0
#     protocol = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#     # allow access to VPC endpoints, not needed for demo
#     prefix_list_ids = []
#   }

#   tags = {
#     Name = "${var.env_prefix}-sg"
#   }
# }

# FOLLOWING reuses default Sec Group of VPC created above
# Basically same config as custom Sec Group above

resource "aws_default_security_group" "myapp-default-sg" {
  vpc_id = aws_vpc.myapp-vpc.id

  ingress {
    description = "SSH into EC2"
    # define port range to open to traffic
    from_port = 22
    to_port = 22
    protocol = "tcp"
    # set your IP to access port(s) opened above
    cidr_blocks = [var.my_ip]
  }

  ingress {
    description = "Browser access"
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    # any IP can access from browser
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "All outbound traffic to Internet"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    # allow access to VPC endpoints, not needed for demo
    prefix_list_ids = []
  }

  tags = {
    Name = "${var.env_prefix}-default-sg"
  }
}

data "aws_ami" "latest-amzn-linux-img" {
  most_recent = true
  # Owner alias (not owner, which is a 12-digit #)
  owners = ["amazon"]

  # TMP/HACK: though similar to hard-coding id, could not find another attr (arn, etc) to filter by that fetched QuickStart AMI ("Amazon Linux 2023 AMI")
  filter {
    # Reference: https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami#attribute-reference > name below must match attr but underscores should be changed to dashes
    name = "image-id"
    values = ["ami-0a3c3a20c09d6f377"]
  }

  # following filters return latest (check Creation Date in EC2 > AMIs > Public images) AMI owned by AMZN whose name matches regex, but it's NOT QuickStart AMI
  # img returned is "Amazon ECS-Optimized Amazon Linux 2023 (AL2023) x86_64 AMI", which may work/ be better than QuickStart, but using QuickStart AMI for demo
  # filter {
  #   # filtering by AMI name
  #   name = "name"
  #   values = ["al2023-ami-*-kernel-6.1-x86_64"]
  # }
  # filter {
  #   name = "virtualization-type"
  #   values = ["hvm"]
  # }
}

# Following used to validate AMI query result
# output "aws_ami_obj" {
#   value = data.aws_ami.latest-amzn-linux-img
# }

resource "aws_key_pair" "myapp-ssh-key" {
  key_name = "${var.env_prefix}-ssh-key"
  # public_key = var.public_key
  
  # Reference: https://developer.hashicorp.com/terraform/language/functions/file, reads contents of file at given path, outputs string
  # public_key = "${file("~/.ssh/id_rsa.pub")}"

  public_key = file(var.public_key_path)
}

resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amzn-linux-img.id
  instance_type = var.instance_type

  subnet_id = aws_subnet.myapp-subnet-1.id
  # following is optional, since subnet is in AZ
  availability_zone = var.az

  # use following (NOT security_groups) for sg's in a non-default VPC
  vpc_security_group_ids = [aws_default_security_group.myapp-default-sg.id]

  # associate SSH key pair w/ EC2 being created (enables SSH access)
  # key_name = "tf-server-key-pair"
  key_name = aws_key_pair.myapp-ssh-key.key_name

  associate_public_ip_address = true

  # References: https://docs.docker.com/engine/install/linux-postinstall/, https://phoenixnap.com/kb/docker-permission-denied, https://developer.hashicorp.com/terraform/language/expressions/strings#heredoc-strings
    # <<-, NOT <<. The hyphen is required for indented cmds to work in multiline str.
    # 1st "sudo systemctl start docker" fails to start daemon but creates docker.sock in /var/run.
    # Adding ec2-user to docker group ("sudo usermod -aG docker $USER"), changing owner of /var/run/docker.sock from root group to docker group ("sudo chown root:docker /var/run/docker.sock"), and "sudo systemctl start docker" didn't start the daemon. Changing ownership of the socket to ec2-user group worked.
    # Must use "ec2-user", NOT $USER, or ownership won't be changed from root.

  # user_data = <<-EOF
  #               #!/bin/bash
  #               sudo yum update -y
  #               sudo yum install -y docker
  #               sudo systemctl start docker
  #               sudo chown ec2-user /var/run/docker.sock
  #               sudo systemctl start docker
  #               docker run --name nginx -p 8080:80 nginx
  #             EOF

  # user_data = file("./entrypt-script.sh")

  tags = {
    Name = "${var.env_prefix}-server"
  }

  # provisioner "local-exec" {
  #   # FOLLOWING DIR MUST HAVE ALL ANS PROJ CONFIG! (hosts, ansible.cfg, vars_files)
  #   working_dir = "/Users/upasananatarajan/Documents/TWNDevOps/15/docker-deploy-demo"
  #   # References: https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html
  #   command = "ansible-playbook --inventory ${self.public_ip}, --private-key ${var.private_key_path} --user ec2-user deploy-docker-generic.yaml"
  # }
}

provider "null" {}

resource "null_resource" "ansible-configure-ec2" {
  # when instance IP changes, null_resource replaced and provisioner re-run (Ans config applied)
  triggers = {
    # any key name
    ec2_instance_id = aws_instance.myapp-server.public_ip
  }

  provisioner "local-exec" {
    # FOLLOWING DIR MUST HAVE ALL ANS PROJ CONFIG! (hosts, ansible.cfg, vars_files)
    working_dir = "/Users/upasananatarajan/Documents/TWNDevOps/15/docker-deploy-demo"
    # References: https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html
    # update self to aws_instance.myapp-server
    command = "ansible-playbook --inventory ${aws_instance.myapp-server.public_ip}, --private-key ${var.private_key_path} --user ec2-user deploy-docker-generic.yaml"
  }
}

output "aws_instance_public_ip" {
  value = aws_instance.myapp-server.public_ip
}